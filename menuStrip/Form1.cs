//#define OLD 
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace gdiDrawLine {
    public partial class Form1 : Form {
        Point[] points;
        Pen pen;
        Pen pen1;
        public Form1() {
            pen = new Pen(Brushes.Black, 5); 
            pen1 =   Pens.Red;
            Paint += Draw_Paint;
#if OLD
            ContextMenu  = new ContextMenu();
            Console.WriteLine("ContextMenu version");
#else 
            ContextMenuStrip = new ContextMenuStrip();
            Console.WriteLine("ContextMenuStrip version");
#endif
            MouseClick += Control1_MouseClick;
            Point[] po = {        new Point(5,5), //5, 10
                                  new Point(50,50),
                                   new Point(100,5),
                                   new Point(150, 50),
                                   new Point(200, 5),
                             };
            points = po;
        }
        void ContextMenuStrip_Click(object sender, EventArgs e) {
            Console.WriteLine("MenuIten have been clicked");
        }
        
        void Control1_MouseClick(Object sender, MouseEventArgs e) {
/*
              Console.WriteLine("Position x/y: {0}/{1}", Cursor.Position.X
                                                        , Cursor.Position.Y);
              Console.WriteLine("MouseEventArgs x/y: {0}/{1}", e.X   // 8[] ���������� ������� ����������
                                                        , e.Y);      //
              Console.WriteLine("MouseEventArgs Location x/y: {0}/{1}", e.Location.X
                                                        , e.Location.Y);  // 8[] ���������� ������� ����������
            Point mouseLoc = PointToClient(Cursor.Position);
              Console.WriteLine("mouseLoc x/y: {0}/{1}", mouseLoc.X
                                                        , mouseLoc.Y);
            mouseLoc = e.Location;     */
            Region r = mkRgn();
            if (r.IsVisible(e.Location)) {
#if OLD
                ContextMenu.MenuItems.Clear();
                MenuItem mi = new MenuItem("in Region");
                ContextMenu.MenuItems.Add(mi);
#else 
                ContextMenuStrip.Items.Clear();
                ToolStripMenuItem mi = new ToolStripMenuItem("in Region");
                ContextMenuStrip.Items.Add(mi);
#endif
                mi.Click +=  ContextMenuStrip_Click;
            }
            else {
#if OLD
                ContextMenu.MenuItems.Clear();
                MenuItem mi = new MenuItem("Not in Region");
                ContextMenu.MenuItems.Add(mi);
#else 
                ContextMenuStrip.Items.Clear();
                ToolStripMenuItem mi = new ToolStripMenuItem("Not in Region");
                ContextMenuStrip.Items.Add(mi);
#endif
                mi.Click +=  ContextMenuStrip_Click;
            }
        }
        public void Draw_Paint(object sender, PaintEventArgs ea) {
            Graphics g        = ea.Graphics;
            Text = "gdiDrawLine";
            // ��������� ������ ������
            g.DrawLines(pen, points);
            for (int i = 0; i < points.Length - 1; i++)
            {
               // ����� ������� ������� �����
                g.DrawPolygon(pen1,mkPolygon(points[i]
                      , points[i+1])
                );
            }
        }
        Region mkRgn() {
            GraphicsPath gp = new GraphicsPath();
            int i;
            for (i = 0; i < points.Length - 1; i++)
            {
                gp.AddPolygon(mkPolygon(points[i]
                      , points[i+1])
                ); 
            }
            Region r = new Region(gp);
            return r;
        }
        Point [] mkPolygon (Point a, Point b) {
                Point[] m = new Point[5];
                m[0] = a;
                m[1] = b;
                m[2] = new Point(b.X + 4, b.Y);
                m[3] = new Point(a.X + 4, a.Y);
                m[4] = a; 
                return m;
        }
    }
}