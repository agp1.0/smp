        public static
        Bitmap   getTile ( string url, Bitmap noInet){
                Bitmap ret = noInet;
                try {
                  DateTime st = DateTime.Now;
                  Console.WriteLine(    "getTile: start query: url of tile: '{0}'"
                     , url);
            //      ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                  ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                  HttpWebRequest webReq = 
                         (HttpWebRequest)System.Net.WebRequest.Create(url);  
                  webReq.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
//                  System.Net.WebRequest webReq = System.Net.WebRequest.Create(url);
                  WebResponse webRes = webReq.GetResponse();
                  Stream stream = webRes.GetResponseStream();
                  ret =   new Bitmap(stream);
                  DateTime fi = DateTime.Now;
                  Console.WriteLine( "getTile: end query: {0:0.0} mSecs", (fi-st).TotalMilliseconds   );
                }
                catch (Exception ex){
                   //log.WriteException( ex);
                   Console.WriteLine(
                        " Cannot take one tile for '{0}' query!. I will use my tile. "
                       , url );
                }
                return ret;
        }
