﻿using System;
using System.IO;

partial class help {
    static public void result(string v, bool vFlag){
        string s = string.Format("{0}{1}{2}", vFlag?"":"'",v, vFlag?"":"'");
        Console.WriteLine(s);
    }
    static public  void show(string help, int rc=1) {
       string nm = Path.GetFileNameWithoutExtension(
            System.Windows.Forms.Application.ExecutablePath);
       System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
       System.Version ver = asm.GetName().Version;
       string version = string.Format("{0}.{1}.{2}", ver.Major.ToString(),
                                    ver.Minor.ToString(), ver.Build.ToString());
       Console.Error.WriteLine("{0}.exe (version:{1}): \n{2}", nm, version, help);
       Environment.Exit(rc);
    }
}

