using System;
using System.IO;
using System.Text;

class demo {
  static void Main(string[] args) {
      string s = null;
      string m = "";
      bool vFlag = false;
      for (int i = 0; i < args.Length; i++) {
          if (args[i] == "-?" || args[i] == "/?" || args[i].ToLower() == "-help") {
              help.show("\nAn application for finding the next natural number. " +
                  "\nUsage: " +
                  "\n a.exe[-?/-help][-v] -n XXX " +
                  "\nwhere \n-help: display the current screen" +
                  "\n-?    : display the current screen " +
                  "\n-v    : output the program runtime" +
                  "\n-n XXX: XXX - natural number.");
          }
          else if (args[i].ToLower() == "-v")
              vFlag = true;

          else if (args[i].ToLower() == "-n")
              if (i+1<args.Length)
                  s = args[++i];
              else 
                  help.show ("nothing todo!");
          else 
                  help.show ("nothing todo!");
      }
      DateTime start = DateTime.Now;
      try {
          m = suc(s);
      }
      catch (Exception e) {
           help.show (
               string.Format("function can't be used with '{0}' param:\n '{1}'"
                    , s , e.Message
               )
               , 2
           );
      }
      DateTime end = DateTime.Now;
      help.result(m, vFlag);
      TimeSpan ts = (end - start);
      if (vFlag) {
          Console.Error.WriteLine("\nTime: {0:0.00} secs", ts.TotalSeconds);
      }
  }
  //  ������� -�������� (stub function) suc ��� ������� ����� 
  //  ��. ��������.�� ���������� � ����������� ���������������� ������ ����� � ��� �������
  public static string suc(string num) { 
      return (ulong.Parse(num)+1).ToString(); 
  }
}

