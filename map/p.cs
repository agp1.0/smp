#pragma warning disable 642

using System;
using System.Globalization;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;


namespace geo {
    static class Program {
	    static double[] resolution;
        static  Program (){
           resolution = new double[19];
           resolution[0]=  156543.03392804097 ; 
           resolution[1]=  78271.51696402048  ; 
           resolution[2]=  39135.75848201024  ; 
           resolution[3]=  19567.87924100512  ; 
           resolution[4]=  9783.93962050256   ; 
           resolution[5]=  4891.96981025128   ; 
           resolution[6]=  2445.98490512564   ; 
           resolution[7]=  1222.99245256282   ; 
           resolution[8]=  611.49622628141    ; 
           resolution[9]=  305.748113140705   ; 
           resolution[10]= 152.8740565703525   ; 
           resolution[11]= 76.43702828517625   ; 
           resolution[12]= 38.21851414258813   ; 
           resolution[13]= 19.109257071294063  ; 
           resolution[14]= 9.554628535647032   ; 
           resolution[15]= 4.777314267823516   ; 
           resolution[16]= 2.388657133911758   ; 
           resolution[17]= 1.194328566955879   ; 
           resolution[18]= 0.5971642834779395  ; 
        }
        static int getZoom (int meters, ref int  divisor){ //������� ������ � �������� �����
          if  (divisor < 2)      divisor = 2;
          else if (divisor > 20) divisor = 20;
          int zm = 18;
          for (; zm > 0; zm--) {
            if (resolution[zm]*256.0 > meters/divisor  )  { // ����� ������� ����� � ������ �� ���������
              if (zm < 18)
                zm++;
              return zm;
            }
          }
          return zm;
        }
        [STAThread]                
        static void Main(string[] args) {
           //Param 
           DateTime st = DateTime.Now;
          // int ii = 1;
          // string title = "";	
           {
              uint tilesX;            	
              uint tilesY;
              uint picX ;
              uint picY ;
              double flatX;
              double flatY;
              double lat1 =  49.437328 ;
              double lon1 =  32.060387 ;
              int  zoom =  18;  // ����������� � �������

              Console.Error.WriteLine( "latitude/longitude/size {0}/{1}/{2}"
                   , coor2str(lat1), coor2str(lon1), zoom);
             
                {
                   yandexLonLat2XY (out tilesX, out tilesY, out picX, out picY, out flatX, out flatY,
                         lat1, lon1, (short)zoom);
                  Console.WriteLine(
                 "wget \"http://vec01.maps.yandex.net/tiles?l=map&v=2.28.0&y={2}&x={1}&z={0}&lang=ru-RU\" -O yandex.{0}.{1}.{2}.png"
                   , zoom , tilesX, tilesY);                                                                 
                }
                {
                   LonLat2XY (out tilesX, out tilesY, out picX, out picY, out flatX, out flatY,
                         lat1, lon1, (short)zoom);
                   Console.Error.WriteLine(
                          "numbers of tile x/y: {0}/{1}; pixel coordinates x/y: {2}/{3}; zoom: {4}"
                        , tilesX, tilesY, picX, picY, zoom);
                   Console.Error.WriteLine(

                          "flat coor x/y: {0}/{1}; "
                        , coor2str(flatX), coor2str(flatY));
                   Console.WriteLine(
                  "wget http://a.tile.openstreetmap.org/{0}/{1}/{2}.png -O osm.{0}.{1}.{2}.png", zoom, tilesX, tilesY);
                }
           }
        }
        static void  yandexLonLat2XY (out uint tilesX
                              , out uint tilesY
                              , out uint picX
                              , out uint picY
                              , out double flatX
                              , out double flatY
                              , double lat
                              , double lon
                              , short  z
                              ) {
           double pi = 3.1415926535897932;
           double latrad = (lat*pi)/180.0;
           double lonrad = (lon*pi)/180.0;

           double a = 6378137.0;
           double k = 0.0818191908426;

           double f =   
                 Math.Tan( pi/4.0 + latrad/2.0) 
                     /
                    Math.Pow(   Math.Tan(pi/4.0 
                             + Math.Asin(k * Math.Sin(latrad)  ) 
                               /2.0
                    ) , k);
           flatX = (20037508.342789 + a *lonrad)     *53.5865938 /  Math.Pow(2, 23-z) ;
           flatY = (20037508.342789 - a *Math.Log(f))*53.5865938 /  Math.Pow(2, 23-z) ;
           Console.Error.WriteLine("flatY {0}/{1}",flatY, coor2str(flatY));

           tilesX = (uint)(flatX/256);
           tilesY = (uint)(flatY/256);
           picX  =  ((uint)flatX)%256;
           picY  =  ((uint)flatY)%256;
        }
        static void  LonLat2XY (out uint tilesX
                              , out uint tilesY
                              , out uint picX
                              , out uint picY
                              , out double flatX
                              , out double flatY
                              , double lat
                              , double lon
                              , short  zoom
                              ) {
           double pi = 3.1415926535897932;
           double latrad = (lat*pi)/180.0;
           Console.Error.WriteLine("lat  radian/input {0}/{1}",latrad, lat);
           double lonrad = (lon*pi)/180.0;

         //  double a = 6378137.0;
         //  double k = 0.0818191908426;
//           double bm0  = 256 *  Math.Pow( 2.0, (double)(zoom/2));
           double bm0  = 256 *  Math.Pow( 2.0, (double)zoom)/2.0;
           Console.Error.WriteLine("bm0/zoom {0}/{1}",bm0, zoom);
           flatX = (bm0 * (1+ lonrad/pi));
           flatY = (bm0 * (1- 0.5 * Math.Log(
                            (1+ Math.Sin(latrad))
                                   /
                                    (1-Math.Sin(latrad)) 
                                           )/pi));
           Console.Error.WriteLine("flatY {0}/{1}",flatY, coor2str(flatY));
           tilesX = (uint)(flatX)/256;
           tilesY = (uint)(flatY)/256;
           picX  =  (uint)(flatX)%256;
           picY  =  (uint)(flatY)%256;
        }
        static string coor2str(double coor) {
           return string.Format("{0:##.######}", coor);
        }
}}                              
