using System;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Reflection;

public class frmMain : Form 
{
	[STAThread]
	static void Main() 	{
		Application.Run(new frmMain());
	}

    Image[]  arrImages       = new Image[4];
    int      intCurrentImage = 0;
    Font     f ;
    Rectangle  r;          // усложненное



    public frmMain()   {
//        this.components = new System.ComponentModel.Container();
//        this.tmrAnimation = new System.Windows.Forms.Timer(this.components);
        System.Windows.Forms.Timer
        tmrAnimation          = new System.Windows.Forms.Timer();
        tmrAnimation.Enabled  = false;
        tmrAnimation.Enabled  = true;
        tmrAnimation.Interval = 500;

        tmrAnimation.Tick += TimerOnTick;

	    f = new Font("Times New Roman", 14);

	    Console.WriteLine("load");
//		global::System.Resources.ResourceManager rm 
		System.Resources.ResourceManager rm 
                    = new global::System.Resources.ResourceManager(
                    "picslst"
                    , typeof(frmMain).Assembly);

	    for (int i=0;i<=3;i++)   		{
			string sFileName =  "pic" + (i + 1).ToString();
  			arrImages[i]= (Image)rm.GetObject(sFileName);	
		}

	    r = new Rectangle
	            (0, 0, ClientSize.Width, ClientSize.Height);
		                      
    }

    protected override void OnPaint(PaintEventArgs ea)	{
            Graphics grfx = ea.Graphics;
	        grfx.FillRectangle(Brushes.Gray, r);
            grfx.DrawString( string.Format("image No {0}", intCurrentImage)
              ,f
              ,Brushes.Red
              ,10 ,3* 14
            );
            grfx.DrawImage(arrImages[intCurrentImage],
                Convert.ToInt32((ClientSize.Width - arrImages[intCurrentImage].Width) / 2), 
                Convert.ToInt32((ClientSize.Height - arrImages[intCurrentImage].Height) / 2), 
                arrImages[intCurrentImage].Width, arrImages[intCurrentImage].Height);
    }


    // This subroutine handles the Tick event for the Timer. 
    // This is where the animation takes place.
    void TimerOnTick(object obj , EventArgs ea ) 	{
            Invalidate();
            intCurrentImage ++;
            if (intCurrentImage >3 )
               intCurrentImage=0;
    }

    // This method overrides the OnResize method in the base Control class. OnResize 
    // raises the Resize event, which occurs when the control (in this case, the 
    // Form) is resized.
    ////  усложненное
    protected override void OnResize(EventArgs ea)	{
        	r = new Rectangle
	            (0, 0, ClientSize.Width, ClientSize.Height);
    }

}