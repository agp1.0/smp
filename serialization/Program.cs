using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;

using Args;

namespace csv2xml {
    enum Formater {Xml, Bin, Soap};

    class Program {
        static public ArgStr iPath;
        static public ArgStr oPath;
        static public ArgFlg hlpF;
        static public ArgFlg vF;
        static public ArgStr fld;
        static public ArgStr  frmt ;
        static public ArgStr sep;
        static public Dictionary<string, string> fldDict;

        static Program() {
            hlpF = new ArgFlg(false, "?", "help", "to see this help");
            vF = new ArgFlg(false, "v", "verbose", "additional output");
            iPath = new ArgStr(".", "i", "input", "input xml path", "PATH");
            iPath.show = false;
            oPath = new ArgStr(".", "o", "output", "output xml path", "PATH");
            oPath.show = false;
            sep = new ArgStr(";", "s", "separator", "csv separator", "CHAR");
            fld = new ArgStr("field", "f", "node attribute", "key:value", "LISTDICT");
            frmt =  new ArgStr("Xml",    "ser", "serialization", "Xml/Bin/Soap:  serialization param"
                                , "SSS");

        }
        static public void usage() {
            Args.Arg.mkVHelp(
            "to demo serialization  "
            , "-i PATH | -o PATH  ", vF, vF, hlpF, frmt, iPath, oPath, sep );
            Environment.Exit(1);
        }

        public static void xml2csv(string inPath, string separator, Formater f) {
           switch (f){
               case Formater.Xml:   xml2csv (inPath,  separator);    break;
               case Formater.Bin:   bin2csv (inPath,  separator);    break;
               case Formater.Soap:  soap2csv(inPath,  separator);    break;
           }
        }

        public static void xml2csv(string inPath, string separator) {
            XmlSerializer formatter = new XmlSerializer(typeof(L));
            using (FileStream fs = new FileStream(inPath , FileMode.Open))
            {         // ��� ������� ����������� �����

                L lst =    (L) formatter.Deserialize(fs);
                foreach (Record r in lst )
                   Console.Out.WriteLine(r.ToString());
            }
        }
        public static void soap2csv(string inPath, string separator) {
            SoapFormatter formatter = new SoapFormatter();

            using (FileStream fs = new FileStream(inPath , FileMode.Open)) {  
                      // ��� ������� ����������� �����

                L lst =    (L) formatter.Deserialize(fs);
                foreach (Record r in lst )
                   Console.Out.WriteLine(r.ToString());
            }   
        }
        public static void bin2csv(string inPath, string separator) {
            
            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream fs = new FileStream(inPath , FileMode.Open)) { 
               // ��� ������� ����������� �����
                L lst =    (L) formatter.Deserialize(fs);
                foreach (Record r in lst )
                   Console.Out.WriteLine(r.ToString());
            }
        }

        public static void csv2xml(string outPath, string separator, Formater f) {

          using (FileStream fs = new FileStream(outPath , FileMode.Create)) {
              L lst = new L();

              string str = Console.In.ReadLine();
              while (str != null)
              {
                  string[] values = str.Split(separator[0]);
                  uint a = 0;
                 double b = 0.1;
                 if (uint.TryParse(values[1], out a))                 ;
                 if (double.TryParse(values[2], out b))                 ;
                 Record r = new Record(values[0], a, b);
                 lst.Add(r);
                  str = Console.In.ReadLine();
              }
           
              switch (f){
                  case Formater.Xml:   csv2xml(lst, fs,  separator);    break;
                  case Formater.Bin:   csv2bin(lst, fs,  separator);    break;
                  case Formater.Soap:  csv2soap(lst, fs, separator);    break;
              }
           }
        }
        public static void csv2xml(L lst, FileStream fs, string separator) {
              XmlSerializer formatter = new XmlSerializer(typeof(L));
              formatter.Serialize(fs, lst);
        }
        public static void csv2bin(L lst, FileStream fs, string separator) {
              BinaryFormatter formatter = new BinaryFormatter();
              formatter.Serialize(fs, lst);
        }
        public static void csv2soap(L lst, FileStream fs, string separator )  {
              SoapFormatter formatter = new SoapFormatter();
              formatter.Serialize(fs, lst);
        }
        public static void splitFiled(string field) {
            if (Object.ReferenceEquals(fldDict, null))
                fldDict = new Dictionary<string, string>();
            string[] val = field.Split(':');
            if (fldDict.ContainsKey(val[0]))
                return;
            fldDict.Add(val[0], val[1]);
        }

        static void Main(string[] args) {
            Formater f;
            for (int i = 0; i < args.Length; i++) {
                if (hlpF.check(ref i, args))
                    usage();
                else if (iPath.check(ref i, args))
                    ;
                else if (oPath.check(ref i, args))
                    ;
                else if (sep.check(ref i, args))
                    ;
                else if (frmt.check(ref i, args))
                    ;
                else if (fld.check(ref i, args)) {
                    splitFiled(fld);
                }
            }
            if (Enum.TryParse(frmt, out f)) { 
                if (oPath != ".") {
                  csv2xml(oPath, sep, f);
                }
                else if (iPath != ".")  {
                  xml2csv(iPath, sep, f);
                }
                else {
                    Console.WriteLine("You should chose key -i\\-o");
                }
            }
            else {
                    Console.WriteLine("wrong walue of serialization {0}", (string)frmt);
                                                      
            }
        }
    }
}
