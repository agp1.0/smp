using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using Args;

namespace csv2xml {
  [Serializable]
  public class L: List<Record>   { }
  [Serializable]
  public class Record   {
      public string Word;
      public uint Amount;
      public double xx;
      private   int sss;
      private static  int count;
     static  Record(){
         count = 0;
     }
     public Record(string _Word, uint _Amount, double _xx) {
          Word = _Word;
          Amount = _Amount;
          xx = _xx;
          count ++ ;
          sss = count;
      }
      public Record() : this("", 0, 1.1) { }
     public override string ToString(){
       return string.Format("{0};{1};{2};{3}", Word, Amount, xx, sss);
     }
  }
}