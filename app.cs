#pragma warning disable 219

using System;

class application1{

 static void Main() {
   int     b   = 2;
   int     i   = 0;
   double  f   = 0.0;
   string  s   =  "none";
   string  q   =  "none";

#if Q1
    q= "����� �������";
   
    int [,] a = new int [3,4];
    i = a.Length;

#elif Q2
    q= "����������� �������";

    int [,] a = new int [3,4];
    i = a.Rank;

#elif Q3
    q= "����� ������ �����������";

    int [,] a = new int [3,4];
    i = a.GetLength(0);
#elif Q4
    q= "����� ������ �����������";

    int [,] a = new int [3,4];
    i = a.GetLength(1);

#elif Q5
    q= "��������� �����";

    i = b++;

#elif Q6
    q= "��������� ��";
    i = ++b;

#elif Q7
   q= "�������� + ���������  �����";
   i = b + b++;

#elif Q8
   q= " ���������  �����  � �������� ";
   i =  b++ + b;

#elif Q9
   q= " ��������� �� ";
   i += b;

#elif Q10
   q= " ��������� �� ";
   i -= b;

#elif Q11
   q= " ����� �������";
   i =  1 / 2;

#elif Q12
   q= " ������� �� �������";
   i =  1 % 2;

#elif Q13
   q= " ������ ����������";
   f = -1.0;
   f = Math.Abs(f);

#elif Q14
   q= " ���������� ����������";
   f = -3.14159;
   f = Math.Round(f);
   i = (int)f;

#elif Q15
   q= " ���������� -2 � ������� 2";
   f = -2.0;
   f = Math.Pow(f, 2);
#elif Q16
   q= " ����� ��";
   f = Math.PI;

#elif Q17
   q= " ������� ������ �� 4";
   f   = 4.0;
   f = Math.Sqrt(f);

#elif Q18
   q= " ����� 0";
   f = Math.Sin(0.0);

#elif Q19
   q= " ������� 0";
   f = Math.Cos(0.0);
#elif Q20
   q= " ��������� ��";
   f = true?2.0:0.0;
#elif Q21
   q= " ��������� ���";
   f = false?2.0:0.0;
#elif Q22
   q= " ������� ���";
   int[] z=null;
   f = z==null?2.0:0.0;
#elif Q23
   q= " ������ ����";
   int [] z = new int[1];
   f = z==null?2.0:0.0;

#elif Q24
   q= " ��������� 3";
   f = double.Parse("3");

#elif Q25
   q= "  ������ ";
   f = double.Parse(" 2 + 3");

#elif Q26
   q= " ��������� 7.0";
   f = 1.0 + 2.0 * 3;

#elif Q27
   q= " ��������� 9.0";
   f = (1.0 + 2.0) * 3;

#elif Q28
   q= " ��������� / ++ ����� 9";
   i = b++ + b++ + b++;

#elif Q29
   q= " ��������� / ��� ++ �� 12";
   i = ++b + ++b + ++b;


#elif Q30
  int    []   a = {-1, 1, 2, 3, -4};    // ������ �������������
              a = new int[3] {11,-22,-33};// �� ���� �����������
  char   [,]  d = new  char [3,2]   // ������ �� 6 char
                      {{'c','d'},{'s','a'},{'q','w'}};
  double [][] c = new  double [2] [];// ������ �� 2� ��������.
  c[0]          = new double []     {1.0, 2.0};
  c[1]          = new double []{1.1, 2.2, 3.3};

  Console.WriteLine 
      ("rank   of a/d/c: {0}/{1}/{2}", a.Rank, d.Rank, c.Rank);
  Console.WriteLine 
      ("length of a/d/c: {0}/{1}/{2}", a.Length, d.Length, c.Length);

//  a = Array.Empty<int>();
  Console.WriteLine 
      ("length of a: {0}; ", a.Length);
  Array.Sort (a);
  Console.WriteLine 
      ("first more then 0 and its index: {0}/{1}; "
               , Array.Find(a, moreThen0)
                  , Array.IndexOf(a, Array.Find(a, moreThen0)));
  foreach (char e in d) {
     Console.Write("{0} ", e );
  }


  Environment.Exit(30);

#else 

#error  You have to set at least one macro

#endif 
   Console.WriteLine("{0} -  i:{2}; f:{3:0.000}", q, s, i, f);

}
static bool moreThen0 (int x){
     return x > 0;
}



}


