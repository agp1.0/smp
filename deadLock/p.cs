using System;
using System.Threading;
class DBC{
  static void Main (){
     DBC dlc = new DBC();
//     
     //     
Monitor.Enter(dlc); {   // there  is dead lock
     //lock(dlc ) {     // there is not dead lock
       dlc = null;
//       Monitor.Exit(dlc);    

       GC.Collect();
       GC.WaitForPendingFinalizers(); //������������� ������ 
                                      // �� ��������� ������ �������� ������
       Console.WriteLine("----");
    }
  }
  ~DBC(){
     lock(this){
        Console.WriteLine("finalizer");
     }
  }
}

