﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SMP_2_2 {
    class Program {
        [STAThread]
        static void Main(string[] args) {
            Application.Run(new Form1());
        }
    }
    class Form1 : Form {
        Rectangle player1;
        Rectangle player2;
        Rectangle ball;
        Region player1_reg;
        Region player2_reg;
        Region ball_reg;

        int player1_points;
        int player2_points;
        bool player1_up;
        bool player1_down;
        bool player2_up;
        bool player2_down;
        int delta_x;
        int delta_y;

        Timer gameTime;
        Timer moveTime;

        public Form1() {
            MinimizeBox = false;
            MaximizeBox = false;
            gameTime = new Timer();
            gameTime.Interval = 20;
            moveTime = new Timer();
            moveTime.Interval = 20;
            player1_points = 0;
            player2_points = 0;

            this.Height = 600;
            this.Width = 800;
            this.BackColor = Color.White;
            this.Paint += new PaintEventHandler(Draw1);
            this.KeyDown += new KeyEventHandler(KeyPressed );
            this.KeyUp += new KeyEventHandler  (KeyReleased);
            gameTime.Tick += new EventHandler(OnGameTick);
            moveTime.Tick += new EventHandler(OnMoveTick);

            if (MessageBox.Show(
               " Press 'OK' to start."
                 ,"Starting the game",MessageBoxButtons.OK
                )==DialogResult.OK) {
                ResetGame();
            }
        }
        public void Draw1(object sender, PaintEventArgs e) {
            Graphics myGraphics = e.Graphics;
            myGraphics.FillRegion(Brushes.Blue, player1_reg);
            myGraphics.FillRegion(Brushes.LimeGreen, player2_reg);
            myGraphics.FillRegion(Brushes.Red, ball_reg);
        }
        public void OnGameTick(object sender, EventArgs e) {
            MoveBall();
            if (ball.IntersectsWith(player2))            {
                delta_x *= -1;
                if (player2_up) {
                    delta_y -= 2;
                }
                if (player2_down) {
                    delta_y += 2;
                }
            }
            if (ball.IntersectsWith(player1)) {
                delta_x *= -1;
                if (player1_up)
                    delta_y -= 2;
                if (player1_down)
                    delta_y += 2;
            }
            if (ball.Y > Height-60) 
                delta_y *= -1;
            if (ball.Y < 0) 
                delta_y *= -1;
            if (ball.X < -10) {
                player2_points++;
                StopGame();
            }
            if (ball.X > Width) {
                player1_points++;
                StopGame();
            }
        }
        public void KeyPressed(object sender, KeyEventArgs e) {
            switch (e.KeyCode) {
                case Keys.W:
                    player1_up = true;
                    break;
                case Keys.S:
                    player1_down = true;
                    break;
                case Keys.Up:
                    player2_up = true;
                    break;
                case Keys.Down:
                    player2_down = true;
                    break;
            }
            if (player1_up || player1_down || player2_up || player2_down) {
                moveTime.Start();
            }
        }
        public void KeyReleased(object sender, KeyEventArgs e) {
            switch (e.KeyCode) {
                case Keys.W:
                    player1_up = false;
                    break;
                case Keys.S:
                    player1_down = false;
                    break;
                case Keys.Up:
                    player2_up = false;
                    break;
                case Keys.Down:
                    player2_down = false;
                    break;
            }
        }
        public void ResetGame() {
            player2 = new Rectangle(Width - 85, Height / 2 - 50, 15, 100);
            player2_reg = new Region(player2);
            player1 = new Rectangle(70, Height / 2 - 50, 15, 100);
            player1_reg = new Region(player1);
            ball = new Rectangle(Width / 2 - 10, Height / 2 - 10, 20, 20);
            GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(ball);
            ball_reg = new Region(gp);
            Invalidate();
            
            delta_x = 10;
            delta_y = 0;
            player2_up = false;
            player2_down = false;
            player1_up = false;
            player1_down = false;
            gameTime.Start();
        }
        public void MoveBall() {
            Rectangle temp = ball;
            ball.Location = new Point(ball.Location.X + delta_x, ball.Location.Y + delta_y);
            GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(ball);
            ball_reg = new Region(gp);
            Invalidate(ball);
            Invalidate(temp);
        }
        public void OnMoveTick(object sender,EventArgs e) {
            bool working = false;
            if (player1_up && player1.Y > 2) {
                Rectangle temp = player1;
                player1.Location = new Point(player1.X, player1.Y -10);
                player1_reg = new Region(player1);
                Invalidate(player1);
                Invalidate(temp);
                working = true;
            }
            if (player1_down && player1.Y < Height - 140) {
                Rectangle temp = player1;
                player1.Location = new Point(player1.X, player1.Y + 10);
                player1_reg = new Region(player1);
                Invalidate(player1);
                Invalidate(temp);
                working = true;
            }
            if (player2_up && player2.Y > 2) {
                Rectangle temp = player2;
                player2.Location = new Point(player2.X, player2.Y - 10);
                player2_reg = new Region(player2);
                Invalidate(player2);
                Invalidate(temp);
                working = true;
            }
            if (player2_down && player2.Y < Height - 140) {
                Rectangle temp = player2;
                player2.Location = new Point(player2.X, player2.Y + 10);
                player2_reg = new Region(player2);
                Invalidate(player2);
                Invalidate(temp);
                working = true;
            }
            if (!working)
                moveTime.Stop();
        }
        void StopGame(){
            gameTime.Stop();
            moveTime.Stop();

            DialogResult result = MessageBox.Show(
             "Score is " + player1_points 
                + ":" + player2_points + "\nWant to continue?"
                  , "Continue?", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)             
                ResetGame();
            else if (result == DialogResult.No)            {
                string ending_str = "";
                if (player1_points > player2_points)    
                    ending_str = "Player 1 won!";
                else if (player1_points < player2_points)   
                    ending_str = "Player 2 won!";
                else
                    ending_str = "It's a tie!";
                MessageBox.Show(ending_str, "Game is finished");
                this.Close();
            }
        }
    }
}