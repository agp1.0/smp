using System;
class Program
{
//event    // ���� ��� � ������ �������, � ���������� �������������
  delegate double func (double l, double r); // ���������� ��� - �������
           // ��� ����������� - �������� ���� func
  static   double sum  (double l, double r) {return l + r;}
  static   double mult (double l, double r) {return l * r;}
  static void Main()
  {
      double l = 2.0, r = 3.0;
      func x = null;   // ���������� ���������� - �������
      x += sum;        // �������� ���������� � ������
      Console.WriteLine("����� {0} � {1} = {2}", l, r
            , x(l, r)  // ��������� �����
            );
      x -= sum; 
      x = null;        // ��� event delegate ������ �������� ������ ������������
      x += mult; 
      Console.WriteLine("������������ {0} � {1} = {2}", l, r
           , x(l, r)   // ��������� ������������
           );
  }
}
