#pragma warning disable 642
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Args;

namespace csv2xml {
    partial class  p {
        static public ArgStr oPath;
        static public ArgFlg hlpF;
        static public ArgChar sep;
        static public ArgStr enc;
        static public ArgStr fNm;

        static public Dictionary<int, string> fldDict;

        static p() {
            hlpF     = new ArgFlg(false, "?", "help", "to see this help");
            oPath    = new ArgStr(".", "o", "output", "output xml path", "PATH");
            oPath.required = true;       // ��� ���������� ������
            sep      = new ArgChar(',', "s", "separator", "csv separator", "CHAR");
            enc      = new ArgStr("866", "e", "encoding", "encoding of standart input (866, 1251, UTF-8)", "ENC");
            fNm      = new ArgStr("field", "f", "fieldName", "to set name of field(number:name)", "FLDNM");
            fNm.show = false;
            fldDict  = new Dictionary<int, string>();
        }
        static public void usage() {
            Args.Arg.mkVHelp(
            "to convert  some csv-file with specified encoding to xml-file "
             , "[-f FLDNM1 [-f FLDM2] ...]", false 
               , hlpF,  oPath, fNm, enc, sep);
            Environment.Exit(1);
        }

        static 
        void Main(string[] args)  {
            bool vFlag=false;
            for (int i = 0; i < args.Length; i++) {
                if (hlpF.check(ref i, args))
                    usage();
                else if (oPath.check(ref i, args))
                    ;
                else if (sep.check(ref i, args))
                    ;
                else if (enc.check(ref i, args))
                    ;                                                
                else if (fNm.check(ref i, args))
                    addNm(fNm, fldDict);
            }
            string enco = "cp866";
            switch (((string)enc).ToLower()){
            	case "866":   enco = "cp866";        break;
            	case "1251":  enco = "windows-1251"; break;
            	case "utf-8": enco = "utf-8";       break;
	    	}
            Console.InputEncoding = Encoding.GetEncoding( enco );
            if (vFlag) {
               string []flds = getRec("");
               if (flds == null)
                  Console.Error.WriteLine("isNull");
               else 
                  Console.Error.WriteLine("length {0} ", flds.Length);
            }
            csv2xml((char)sep);
        }

        static 
        string [] getRec (string line,  char sep= ' ') {
           string [] flds=null;
           char [] seps;
           if (line != null) {
             // ������� ����������, ���������� � ����������� �������
             int lastP = line.IndexOf('#');
             if (lastP >= 0)     // ������� ����������
                 line = line.Substring(0, lastP);
             line = line.Trim(); // ������� �������

             if (sep == ' ') {  // � ������ ������� ��������
                                // ���������
                seps = new char[2];
                seps[0] = ' ';
                seps[1] = '\t';
                flds = line.Split
                   (seps, StringSplitOptions.RemoveEmptyEntries);
             }
             else { // ������ ������ ����������� ������� ��������� ����
                seps = new char[1];
                seps[0] = sep;
                flds = line.Split (seps);
             }
           }
           return flds;
        }
    }
}
