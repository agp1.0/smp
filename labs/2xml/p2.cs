﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Args;

namespace csv2xml {
    partial class p {
	static 
	void csv2xml(char sep1) {
            string outPath = oPath;
            char separator = sep1;
            if (outPath != ".") {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<Table></Table>");
                XmlNode tbl = xd.DocumentElement;
                using (TextReader tr = Console.In ){
                  string str = null;
                  int  recNo = 0;
                  string[] values;
                  XmlNode attr ;
                  XmlNode fld ;
                  XmlNode rec ;
                  string fNm =null;
                  while ((str=tr.ReadLine()) != null) {
                      values  = getRec (str,  sep1);
                      if(values.Length == 0)   continue;
                      if(values.Length == 1 
                        && values[0].Length <=0 )   continue;

                      recNo++;
                      rec = xd.CreateElement("Rec");
                      attr = xd.CreateNode(XmlNodeType.Attribute, "No", null);
                      attr.Value = recNo.ToString();
                      rec.Attributes.SetNamedItem(attr);
                      for (int i = 0; i < values.Length; i++)
                      {
                          if (fldDict != null && fldDict.ContainsKey(i)) {
                             fNm =  fldDict[i];
                          }
                          else 
                             fNm = string.Format("field_{0}", i);
                          fld = xd.CreateElement(fNm);
                          fld.InnerText = values[i];
                          rec.AppendChild(fld);
                      }
                      tbl.AppendChild(rec);
                  }
                }
                xd.Save(outPath);
            }
       	    else    {
       			Console.Error.WriteLine("You have to set name of xml-file");
       	    }
        }

        static 
        void addNm(string field, Dictionary<int, string> fldDict) {
            string[] val = field.Split(':');
            try {
              int k = int.Parse(val[0]); 
              if (fldDict.ContainsKey(k) )
                  return;
              fldDict.Add(k, val[1]);
            }
            catch {
                Console.Error.WriteLine("wrong parameter: {0}", field);
	    }
        }
    }
}
