using System;

namespace ConsoleApplication1
{
    class  cc {
       public 
       int p;
    }
    class Program   {
        [STAThread]
        static void Main(string[] args)
        {
           cc a = new cc(), b = new cc(), c = new cc(), d = new cc();
           a.p = 1;
           b.p = 2;
           c.p = 3;
           d.p = 4;

           Console.WriteLine("a/b/c/d before f : {0}/{1}/{2}/{3}", 
               a.p, b.p, c.p, d.p);
           f(a, ref b, c, ref d);
           Console.WriteLine("a/b/c/d after  f : {0}/{1}/{2}/{3}",
               a.p, b.p, c.p, d.p);
        }

        static void f ( cc p1, ref cc p2, cc p3, ref cc p4){
           p1.p = 10;
           p2.p = 10;
           p3 = new cc();
           p3.p = 10;
           p4 = new cc();
           p4.p = 10;
           Console.WriteLine("p1/p2/p3/p4  inside  f : {0}/{1}/{2}/{3}", 
              p1.p, p2.p, p3.p, p4.p);
        }


    }
}
                                         