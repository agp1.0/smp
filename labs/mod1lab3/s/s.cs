using System;

namespace ConsoleApplication1
{
    struct  ss {
       public 
       int p;
    }
    class Program   {
        [STAThread]
        static void Main(string[] args)
        {
           ss a, b, c, d ;
           a.p = 1;
           b.p = 2;
           c.p = 3;
           d.p = 4;

           Console.WriteLine("a/b/c/d before f : {0}/{1}/{2}/{3}", 
               a.p, b.p, c.p, d.p);
           f(a, ref b, c, ref d);
           Console.WriteLine("a/b/c/d after  f : {0}/{1}/{2}/{3}",
               a.p, b.p, c.p, d.p);
        }

        static void f ( ss p1, ref ss p2, ss p3, ref ss p4){
           p1.p = 10;
           p2.p = 10;
           p3 = new ss();
           p3.p = 10;
           p4 = new ss();
           p4.p = 10;
           Console.WriteLine("p1/p2/p3/p4  inside  f : {0}/{1}/{2}/{3}", 
              p1.p, p2.p, p3.p, p4.p);
        }


        static void f ( int p1, ref int p2, int p3, ref int p4){
           p1 = 10;
           p2 = 10;
           p3 = new int();
           p3 = 10;
           p4 = new int();
           p4 = 10;
           Console.WriteLine("p1/p2/p3/p4  inside  f : {0}/{1}/{2}/{3}", p1, p2, p3, p4);
        }

    }
}
                                         