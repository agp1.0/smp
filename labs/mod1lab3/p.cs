using System;

namespace ConsoleApplication1 {
    class Program     {
        static void Main(string[] args) {
           int a = 1, b = 2, c = 3, d = 4;
           Console.WriteLine("a/b/c/d before f : {0}/{1}/{2}/{3}",a, b, c, d);
           f(a, ref b, c, ref d);
           Console.WriteLine("a/b/c/d after  f : {0}/{1}/{2}/{3}",a, b, c, d);
        }
        static void f ( int p1,            // �������� ������������ ��������� �� ��������
                          ref int p2,      // �������� ������������ ��������� �� ������
                             int p3,       // �������� ������������ ��������� �� ��������
                              ref int p4){ // �������� ������������ ��������� �� ������
           p1 = 10;
           p2 = 10;
           p3 = new int();    // ��� �������� ����� ��� ������� ������
           p3 = 10;
           p4 = new int();
           p4 = 10;
           Console.WriteLine("p1/p2/p3/p4  inside  f : {0}/{1}/{2}/{3}", p1, p2, p3, p4);
        }

    }
}
                                         