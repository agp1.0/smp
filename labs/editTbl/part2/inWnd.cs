using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ConsoleApplication5
{
    class fld 
    {
        public string name;
        public string def;
        public string value;
        public fld(string nm, string df)
        {
            name = nm;
            def = df;
            value = "";
        }

      }

    class inWnd : OkCancel
    {
        Dictionary<TextBox, fld>   flds = null;    

        public inWnd(string Title, fld[] ps) : base(Title)
        {
            flds = new  Dictionary<TextBox, fld>(); // ������� ������ ���
            for (int i = 0; i< ps.Length; i++)
               flds.Add(addFld (i,  ps[i]), ps[i]);
            butOk.Click +=  _KeyDown;
        }

        public inWnd(string Title, fld a, fld b) : base(Title)
        {
            flds = new  Dictionary<TextBox, fld>(); // ������� ������ ���
            flds.Add(addFld (1,  b), b);         /// �������� ������ ���� ����� 
            flds.Add(addFld (0,  a), a);         /// �������� ������ ���� ����� 
            butOk.Click +=  _KeyDown;
        }

       public TextBox addFld                     /// ����� ��������� � ���� ����� ���� �����
                   (int n,  fld par){
           int h = 32;

           Panel p       =new Panel();               /// ������ �������� ����� � ���� �����
           p.Name        = string.Format("p{0}", n);
           p.BorderStyle = BorderStyle.FixedSingle;
           p.Size        = new Size(1, h);
           p.AutoSize    = true;
           p.Dock        = DockStyle.Top;
           p.Padding     =  new Padding (0,0,0,8);    ///



           Panel p1       = new Panel();               /// ������ ��� ������
           p1.BorderStyle = BorderStyle.FixedSingle;
           p1.Size        = new Size(1, h);
           p1.Dock        = DockStyle.Top;             /// <--------
           p1.BackColor   = Color.Blue;
           p.Controls.Add(p1);

           Label l1;
           TextBox t1;

           l1 = new Label();
           l1.Name     = string.Format("l{0}", n);
           l1.Size     = new Size(172 , h);;
           l1.Text     = par.name;  /// ������� ����� ������
           l1.Dock   = DockStyle.Right ;

//           Console.Error.WriteLine ("new field: '{0}'/'{1}'/'{2}'"
//             , par.name,  l1.Text, par.value);
           l1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
           p.Controls.Add(l1);


           t1 = new TextBox();
           t1.TabIndex = n;
           t1.Name     = string.Format("t{0}", n);;
           t1.Size     = new Size(162 , h);
           t1.Text     = par.def;  
           t1.Dock     = DockStyle.Right ;
           p.Controls.Add(t1);
           Controls.Add(p);           /// ������� ���� �����
           return t1;
       }

       void _KeyDown(object sender, EventArgs e) {
           TextBox tb;
           fld     f;
           foreach (KeyValuePair<TextBox, fld> Item in flds)
           {
               tb = Item.Key;
               f  = Item.Value;
               f.value = tb.Text;
               tb.Text = f.def;       /// � ������ ����������������� �������� �� ���������.
               Console.Error.WriteLine("keyDown: '{0}':'{1}' ", f.name, f.value);
           }
       }
    }
}
