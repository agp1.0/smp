using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;
using ConsoleApplication5;

namespace dtGrd  {
    public class wnd3 : wnd2 {
         public  wnd3(string fnm):base (fnm) {
            dgv.SelectionMode 
             = DataGridViewSelectionMode.FullRowSelect; // �������� ��� ������
            dgv.RowHeadersVisible = false;              // ����� ������� ������ �������

            tb.ButtonClick -= base.ToolBarButtonClick;
            tb.ButtonClick += ToolBarButtonClick3;
            dgv.CellDoubleClick +=  dc2;               // �������������� ������ �� enter
            dgv.PreviewKeyDown  +=  _PreviewKeyDown;
            dgv.KeyDown         +=  _KeyDown;
         }
         void _PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) {
           if (e.KeyCode == Keys.Enter) {
                 Console.WriteLine("CellDoubleClick: selrow / row: {0} "
                 , dgv.CurrentRow.Index);
                       doEdit();
           }     
         }          
         void _KeyDown(object sender, KeyEventArgs e) {
           if (e.KeyCode == Keys.Enter) 
                  e.SuppressKeyPress = true;
         }          
         protected
         void dc2(object sender,  DataGridViewCellEventArgs e) {
             Console.WriteLine("CellDoubleClick: selrow / row: {0}/{1} "
             , dgv.CurrentRow.Index, e.RowIndex);
             if (e.RowIndex >=0) {
               doEdit();
               dgv.CurrentCell = dgv.Rows[e.RowIndex].Cells[0];
             }
         }
         protected
         void ToolBarButtonClick3(object sender, ToolBarButtonClickEventArgs e) {
             string bNm = e.Button.Text;
            statStrip.Items[0].Text 
                        = string.Format("You've pressed {0} button", bNm);
             if (bNm == "Exit")
               Close();
             else if (bNm == "Insert")
               doIns();
             else if (bNm == "Edit")
               doEdit();
             else if (bNm == "Save")
               doSave();
             else 
                 base.ToolBarButtonClick ( sender,  e);
         }
         public void  doEdit() {
             string ss = "";
             if(dgv.RowCount > 0) {
                DataGridViewRow dgvR = dgv.Rows[dgv.CurrentRow.Index];
                DataGridViewCellCollection row = dgvR.Cells;/// ��� �� ������
                List<fld> flds = new List<fld>();
                fld f;
                for (int i = 0; i< dgv.ColumnCount; i++) {
                   f	 = new fld (dgv.Columns[i].Name
                         ,  (row[i]).Value.ToString());
                   flds.Add(f);
                }
                if(flds.Count> 0) {
                    Form w = new inWnd("Edit selected record", flds.ToArray());
                    DialogResult rc = w.ShowDialog();
                    if (rc == DialogResult.OK) {
                      for (int kk = 0; kk< flds.Count; kk++){
                          dgvR.Cells[kk].Value =  flds[kk].value;
                      }
                    }
                }
             }
             else 
                ss = "nothing to dp!";
             Console.WriteLine("Edit: '{0}'", ss);
             statStrip.Items[0].Text = ss;
         }
         public void  doSave() {
             string ss = csvExport  (fileName, ';', dgv);
             Console.WriteLine("Save: '{0}'", ss);
             statStrip.Items[0].Text = ss;
         }
         public void  doIns() {
             fld f ;
             List<fld> flds = new List<fld>();
//              DataGridViewRow dgvR = new DataGridViewRow();
             for (int i = 0; i< dgv.ColumnCount; i++){
                f	 = new fld (dgv.Columns[i].Name, "");
                flds.Add(f);
             }
             if(flds.Count> 0){
                Form w = new inWnd("Input new record", flds.ToArray());
                DialogResult rc = w.ShowDialog();
                if (rc == DialogResult.OK)   {
 //                  fld f1;
 //                  DataGridViewRow dgvR = new DataGridViewRow();
                   Console.Error.WriteLine("doIns: ���-�� �����: '{0}' ", flds.Count);

                   statStrip.Items[0].Text 
                   = string.Format("insert after {0} rec"
                   ,  dgv.CurrentRow.Index);

                   string[] fs = new string[flds.Count];
                   for (int j =0; j<flds.Count; j++ ) {
                     fs[j]  = flds[j].value;
//                      f1 = flds[j];
//                      Console.Error.WriteLine("doIns: keyDown: '{0}':'{1}' ", f1.name, f1.value);
                   }
                   dgv.Rows.Insert(dgv.CurrentRow.Index, fs);
                   dgv.CurrentCell = dgv[1,dgv.CurrentRow.Index-1];
                }
             }
             else {
                statStrip.Items[1].Text 
                     = string.Format("nothing to do!");
             
             }
         }
    }
}    