using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;



namespace dtGrd
{
    public class wnd2 : wnd
    {
         protected
         ToolBar tb = new ToolBar();
         protected
         StatusStrip statStrip;

         public  wnd2(string fnm):base (fnm)
         {
             statStrip = new StatusStrip();
             ToolStripStatusLabel 
             statLabel = new ToolStripStatusLabel();
             statStrip.Items.Add(statLabel);           
             Controls.Add(statStrip);
             statStrip.Items[0].Text = "���� ������";


             tb.ButtonSize = new System.Drawing.Size((int)(200/ 3),  (int)(40)  );

             ToolBarButton tlbExit = new ToolBarButton("Exit");
             tlbExit.ToolTipText = "������� ���� �������";
             ToolBarButton tIns  = new ToolBarButton("Insert");
             tIns.ToolTipText = "�������� ������";
             ToolBarButton tEdit = new ToolBarButton("Edit");
             tEdit.ToolTipText = "������������� ������";
             ToolBarButton tDel  = new ToolBarButton("Delete");
             tDel.ToolTipText = "������� ������";
             ToolBarButton tSave = new ToolBarButton("Save");
             tSave.ToolTipText = "��������� ���������";
             ToolBarButton tExp  = new ToolBarButton("Export");
             tExp.ToolTipText = "�������������� �������";

             Padding = new Padding(2);
             tb.Buttons.AddRange(new ToolBarButton[] { 
                    tIns
                  , tEdit
                  , tDel
                  , tSave
                  , tExp
                  , tlbExit
                  }
             );

            tb.ButtonClick += new ToolBarButtonClickEventHandler(ToolBarButtonClick);


             tb.Dock = DockStyle.Top;
             Controls.Add(tb);
             Load += fLoad;
         }
          void fLoad(object sender, EventArgs a)
          {
             statStrip.Items[0].Text 
             = string.Format("{0} records has been red", dgv.Rows.Count);
          }

          protected
          void ToolBarButtonClick(object sender, ToolBarButtonClickEventArgs e)
          {
              string bNm = e.Button.Text;
             statStrip.Items[0].Text 
             = string.Format("You've pressed {0} button", bNm);

              if (bNm == "Exit")
                Close();
              else if (bNm == "Open")
                ;
              else if (bNm == "Export")
                 export (dgv, ';');
              else {
                  MessageBox.Show("action not ready!","Warning");
              }

          }
          public 
          void export (DataGridView dgvw, char sep){
            SaveFileDialog svFileDialog1 = new SaveFileDialog();
            svFileDialog1.Filter 
    = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt| xml files (*.xml)|*.xml|All files (*.*)|*.*";
            svFileDialog1.FilterIndex = 1;
            svFileDialog1.RestoreDirectory = true;
            svFileDialog1.InitialDirectory 
               = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (svFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fnm = svFileDialog1.FileName;
                string ext = Path.GetExtension (fnm);
                if (ext == ".xml")
                   statStrip.Items[0].Text = xmlExport  (fnm, dgvw);
                else   {
                   string ss = csvExport  (fnm, ';', dgvw);
                   Console.WriteLine("Export: '{0}'", ss);
                   statStrip.Items[0].Text = ss;
                }
            }
          }
          public virtual 
          string xmlExport (string nm, DataGridView dgvw){
              string rc = string.Format ("Can't export data to {0} file", Path.GetFileName(nm));
              MessageBox.Show(rc, "Wrning! XML export it's not ready!");
              return rc;
          }
          public virtual 
          string csvExport (string nm, char sep_,  DataGridView dgvw){
              string rc = "error";
              string sep =  sep_.ToString();
              int rNo = 0;
              using (StreamWriter wr = new StreamWriter(nm, false, Encoding.GetEncoding (1251)))        {
                       DataGridViewCellCollection row;
                       string[] flds;
                       for(rNo = 0; rNo < dgvw.RowCount;  rNo++){
                         row = dgvw.Rows[rNo].Cells;
                         flds = new string[row.Count];
                         for(int cNo = 0; cNo < row.Count ; cNo++){
                           flds[cNo] = (row[cNo]).Value.ToString();
                         }
                         string rec = string.Join(sep, flds);
                         wr.WriteLine(rec);
                       }
                       rc =  string.Format("{0} records has been exported to file {1}"
                              , rNo, Path.GetFileName(nm) );
              }
              return rc;
          }
    }           
}


















