﻿//#define WND
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;
using ConsoleApplication5;


namespace dtGrd
{

    class Program
    {
        [STAThread]
        static void Main(string[] input)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.InitialDirectory 
                = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fnm = openFileDialog1.FileName;
                Application.Run(new wnd3(fnm));
            }
        }
    }
}

