using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;
using ConsoleApplication5;




namespace dtGrd 
{
    public class wnd3 : wnd2
    {
         public  wnd3(string fnm):base (fnm)
         {
            tb.ButtonClick -= base.ToolBarButtonClick;
            tb.ButtonClick += ToolBarButtonClick3;
         }
          protected
          void ToolBarButtonClick3(object sender, ToolBarButtonClickEventArgs e)
          {
              string bNm = e.Button.Text;
             statStrip.Items[0].Text 
             = string.Format("You've pressed {0} button", bNm);
              if (bNm == "Exit")
                Close();
              else if (bNm == "Insert")
                doIns();
              else {
                  base.ToolBarButtonClick
                   ( sender,  e);
              }
          }
//          public void  doIns(object sender, EventArgs arg){
          public void  doIns(){
              fld f ;
              List<fld> flds = new List<fld>();
              for (int i = 0; i< dgv.ColumnCount; i++){
                 f	 = new fld (dgv.Columns[i].Name, "");
                 flds.Add(f);
              }
              if(flds.Count> 0){
                 Form w = new inWnd("Input new record", flds.ToArray());
                 DialogResult rc = w.ShowDialog();
              }
              else {
                 statStrip.Items[1].Text 
                 = string.Format("nothing to do!");
              
              }
          }
    }
}

