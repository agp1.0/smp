using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;


namespace line
{

    class sLine {                                 ///<����� �� ������
        public string   nm        =  "line";
        public Point[] ps;                       ///<���������� ����� ����� � ��������
        public Pen      pen       =  Pens.Black;    ///<�������� ��� ��������� ��������� 
//                                     new Pen(Brushes.Black, 4);
        public bool     mkPolygon = false;
        public bool     mkLine    = true;
        public bool     vFlag     = false; 

        
        public Brush    brush  =  Brushes.Black; ///< �������� ��� �������� ��������
        public Pen      vPen   =  Pens.Blue;     ///<�������� ��� ��������������� ��������� 
        public Brush    vBrush =  Brushes.Blue;  ///< �������� ��� �������������� ��������

        public bool     mkDots =  false;
        public bool     mkcaptions = false;
        public int      lnWidth = 2;
        public int      dotWidth = 3;


        // ��������� �����
        public void _paint (object sender, PaintEventArgs e){
             Graphics g = e.Graphics;
             if (ps != null && ps.Length > 1)  {
//                pen.Width = 2;

//                Console.WriteLine("paint line/len: '{0}'/{1}",nm, ps.Length);
               if (mkPolygon)
                 g.DrawPolygon(pen,  ps);       // ��������� �����
               else 
                 g.DrawLines(pen,  ps);
               if (vFlag)
                 for (int j = 0; j < ps.Length; j++){
                   Console.WriteLine("no/x/y: {0}/{1}/{2}",j, ps[j].X, ps[j].Y);
                 }

             }
             else {
                string text = String.Format("line '{0}' is empty!", nm);
                using (Font f = new Font("Times New Roman", 14)){
                    g.DrawString( text,f
                      , Brushes.Red
                      , 20 ,4* 14
                    );
                }
             }

        }

        public Point []                            // ������ ������� �����
        getPoints(TextReader tr, bool vFlag=false)
        {
            List <Point> ps = new List <Point> ();
            int errs=0;
            int lineNo=0;
            int a;
            int b;
            string r;
            string[] sep = { " ", "\t" }; // separators 

           for ( ;(r = tr.ReadLine())!=null;  lineNo++){
             string[] flds = r.Split(sep, StringSplitOptions.RemoveEmptyEntries);
             if (flds.Length >= 2)
             {
               try {
                 a = int.Parse(flds[0]);
                 b = int.Parse(flds[1]);
                 ps.Add (new Point(a, b));
               }
               catch (Exception ex) {
                  errs++;
                  if (vFlag)
                     Console.Error.WriteLine ( 
                       "lineNo/r/error: {0}/{1}/{2}", lineNo, r, ex.Message
                     );
               
               }
             }
             else if (flds.Length == 1) {
                  errs++;
                  if (vFlag)
                     Console.Error.WriteLine ( 
                       "lineno/r/error: {0}/{1}/{2}", lineNo, r, "not enough data"
                     );
             }
           }
           if (errs>0)
              Console.Error.WriteLine ("There was {0} errors", errs);
           this.ps = ps.ToArray();
           return this.ps;
        }
    }

}
