using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace line
{
    public class mapping
    {
        int l, r,   //  ������� ����
            t, b;   //  b ����, t ���
        
        bool isWellformed = false;
        public double   xMin, xMax, yMin, yMax;  // ����������
        public mapping( ):this(0.0, 1.0, 0.0, 1.0){
            this.l = 0;      
            this.b = 0;    
            this.r = 255;  
            this.t = 255;
        }
        public mapping( double x, double X, double y, double Y)
        {
            xMin = x; 
            xMax = X; 
            yMin = y; 
            yMax = Y;
            if (xMin == xMax)
               xMax = xMin + 0.01;
            if (yMin == yMax)
               yMax = yMin + 0.01;

            this.l = 1;      
            this.b = 1;    
            this.r = 400;  
            this.t = 300;
            isWellformed = true;
        }

        public int w {
          get {return r+l;}    //  ���� ������� l ����� � ������
        }
        public int h {
          get {return t+b;}    //  ���� �������  b ������ � �����
        }
        public
        void map  (double X, double Y,            out int x, out int y){
            if (isWellformed) {
              x = Convert.ToInt32(Math.Round((X - xMin) / (xMax - xMin) * (r-l)+l));
              y = Convert.ToInt32(Math.Round((Y - yMin) / (yMax - yMin) * (t-b)+b));
            }
            else {
              x=0; y=0;
            }
        }

        public void mkZmEqual(){
         if (isWellformed) {
           double xSz = xMax - xMin;
           double ySz = yMax - yMin;
           double correction = 0.0;
           if (xSz > ySz) {
              correction =   (xSz - ySz) / 2.0;
              yMax += correction;
              yMin -= correction;
           }
           else if (xSz < ySz) {
              correction =   (ySz - xSz) / 2.0;
              xMax += correction;
              xMin -= correction;
           }
         }
        }
        public void moveNordSouth ( 
           int  shift               ///< �������� ������ ������� ���������, >0 -- �� �����, <0 -- �� ��
          ){
         if (isWellformed) {
           
             if (shift >  50 )  shift =  50;
             if (shift < -50 )  shift = -50;
             double percent = (yMax - yMin) / 100;
             yMax+= percent*shift;
             yMin+= percent*shift;
         }
        }
        public void moveEastWest ( 
           int  shift               ///< �������� ������ ������� ���������, >0 -- �� �����, <0 -- �� ��
          ){ 
         if (isWellformed) {
             if (shift >  50 )  shift =  50;
             if (shift < -50 )  shift = -50;
             double percent = (xMax - xMin) / 100;
             xMax+= percent*shift;
             xMin+= percent*shift;
         }
        }
        public void zoom  ( 
           int  shift             ///< >0 --���������� ������� ��������� (������ �������),
                                  ///  <0 ���������� ������� ��������� (���������� ��������)
        
        ){
         if (isWellformed) {
          if (shift >  50 )  shift =  50;
          if (shift < -50 )  shift = -50;
          double percentY;
          double percentX;
          if (shift > 0) {
             percentY  =  (yMax - yMin) / 100;
             percentX  =  (xMax - xMin) / 100;
          }
          else {
             percentY   = (yMax -yMin) / ( 100 + 2 * Math.Abs(shift));
             percentX   = (xMax -xMin) / ( 100 + 2 * Math.Abs(shift));
          }
          yMax+= percentY*shift;
          yMin-= percentY*shift;
          xMax+= percentX*shift;
          xMin-= percentX*shift;
         }
        }
    }
}
