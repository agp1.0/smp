using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;  // ��� ���������� �����
using System.Threading;
namespace line
{
    class rLine {     ///< ����� ����������
        public tuple2d[] pnts;                       ///<���������� ����� ����� � ��������
        public string nm; 
        static
        readonly   char NumberDecimalSeparator
              = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];

        public  rLine (string nm, string nordNm ="", string eastNm=""){
          this.nm = nm;
        }

        public bool getBox (out double xMin, out double xMax, out double yMin, out double yMax){
          xMin =  double.MaxValue  ;
          xMax =  double.MinValue  ; 
          yMin =  double.MaxValue  ;
          yMax =  double.MinValue  ;
          if (pnts !=null && pnts.Length > 0){
             for (int i=0; i< pnts.Length; i++){
               if (pnts[i].X < xMin)
                  xMin = pnts[i].X;
               if (pnts[i].X > xMax)
                  xMax = pnts[i].X;
               if (pnts[i].Y < yMin)
                  yMin = pnts[i].Y;
               if (pnts[i].Y > yMax)
                  yMax = pnts[i].Y;
             }
             return true;
          }
          return false;
        }

        public static tuple2d []                            // ������ ������� �����
        getPoints(TextReader tr, bool vFlag=false, char    decPnt = '.')
        {
            List <tuple2d> ps = new List <tuple2d> ();
            int errs=0;
            int lineNo=0;
            string r;
            string[] sep = { " ", "\t" }; // separators 

           for ( ;(r = tr.ReadLine())!=null;  lineNo++){
             if (NumberDecimalSeparator != decPnt)
                 r = r.Replace(decPnt, NumberDecimalSeparator);

             string[] flds = r.Split(sep, StringSplitOptions.RemoveEmptyEntries);
             if (flds.Length >= 2)              {
               try {
                 ps.Add (new tuple2d(flds[0], flds[1]));
               }
               catch (Exception ex) {
                  errs++;
                  if (vFlag)
                     Console.Error.WriteLine ( 
                       "lineNo/r/error: {0}/{1}/{2}", lineNo, r, ex.Message
                     );
               }
             }
             else if (flds.Length == 1) {
                  errs++;
                  if (vFlag)
                     Console.Error.WriteLine ( 
                       "lineno/r/error: {0}/{1}/{2}", lineNo, r, "not enough data"
                     );
             }
           }
           if (errs>0)
              Console.Error.WriteLine ("There was {0} errors", errs);
           return ps.ToArray();
        }
    }

}
