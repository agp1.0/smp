using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ConsoleApplication2 {
    class Program     {
        static void Main(string[] args) {
            using (BinaryReader br = new BinaryReader(File.Open("1.bin", FileMode.Open))) {
                bin2str(br, Console.Out);
            }
        }
       static void bin2str(BinaryReader br, TextWriter tw) {
            int a;
            int b;
            string r;
            string[] sep = { " ", "\t" }; // separators 
            while (br.PeekChar()!=-1) {
                a = br.ReadInt32();
                b = br.ReadInt32();
                tw.WriteLine("{0} {1}", a, b);
            }
        }
    }
}
