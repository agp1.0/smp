using System;
using System.Windows.Forms;


namespace m3l1{
    class Program     {
        static 
        int Main(string[] args)        {
            int x=0;           //  ����������  ������ ��������                 
            int y=0;           //    ���� ������������ �������� ����           
            string w="text";   //  ����� �� ����������� �������� ����          
            int f = 0;         //  �������, ����� ����������� ������� �������� 
            if (args.Length == 0)            {
                Console.Error.WriteLine("Error: Nothing to do!");
                return 1;
            }
                //���� �� ������
            for (int i = 0; i < args.Length; i++) {
                if (args[i].Equals("-?")){
                    Console.WriteLine("     Spravka. Programma dlya vstavki v formu knopki, nadpisi ili tekstovogo polya.");
                    Console.WriteLine("     app.exe [-?] [-x ***] [-y ***] [-w ***] -k <button/label/textbox>;");
                    Console.WriteLine("     -?  spravka;");
                    Console.WriteLine("     -x  otstup obyekta ot levogo kraya;");
                    Console.WriteLine("     -y  otstup obyekta ot verhnego kraya;");
                    Console.WriteLine("     -w  text na obyekte ;");
                    Console.WriteLine("     -k  vibor obyekta(button,label ili textbox);");
                    return 1;        // ��� ������ ���������� ������ ���� ������� �� 0
                }
                else if (args[i].ToLower().Equals("-x"))
                    x = Convert.ToInt32(args[i+1]);
                else if (args[i].ToLower().Equals("-y")) 
                    y = Convert.ToInt32(args[i+1]);
                else if (args[i].ToLower().Equals("-w")) 
                    w = args[i+1];
                else if (args[i].ToLower().Equals("-k")) {
                  if (i+1 < args.Length) {         //*** �������� ������������� ��������� 
                    if (args[i+1].ToLower().Equals("button"))
                        f = 1;
                    else if (args[i+1].ToLower().Equals("label"))
                        f = 2;
                    else if (args[i+1].ToLower().Equals("textbox"))
                        f = 3;
                    else {
                        Console.Error.WriteLine("Error: wrong parameter!");
                        return 1;
                    }
                  }
                  else {
                        Console.Error.WriteLine("Error: missed parameter!");
                        return 1;
                  }
                }
            }
            Application.Run(new wnd(x, y, f, w));
            return 0;
        }
    }
}

