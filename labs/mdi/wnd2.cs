﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;



namespace dtGrd
{
    public class wnd2 : wnd
    {
         ToolBar tb = new ToolBar();
         StatusStrip statStrip;

         public  wnd2(string fnm):base (fnm)
         {
             statStrip = new StatusStrip();
             ToolStripStatusLabel 
             statLabel = new ToolStripStatusLabel();
             statStrip.Items.Add(statLabel);           
             Controls.Add(statStrip);
             statStrip.Items[0].Text = "окно готово";


             tb.ButtonSize = new System.Drawing.Size((int)(200/ 3),  (int)(40)  );

             ToolBarButton tlbExit = new ToolBarButton("Exit");
             tlbExit.ToolTipText = "закрыть окно таблицы";
             ToolBarButton tIns  = new ToolBarButton("Insert");
             tIns.ToolTipText = "добавить запись";
             ToolBarButton tEdit = new ToolBarButton("Edit");
             tEdit.ToolTipText = "редактировать запись";
             ToolBarButton tDel  = new ToolBarButton("Delete");
             tDel.ToolTipText = "удалить запись";
             ToolBarButton tSave = new ToolBarButton("Save");
             tSave.ToolTipText = "сохранить изменения";
             ToolBarButton tExp  = new ToolBarButton("Export");
             tExp.ToolTipText = "экспортировать таблицу";

             Padding = new Padding(2);
             tb.Buttons.AddRange(new ToolBarButton[] { 
                    tIns
                  , tEdit
                  , tDel
                  , tSave
                  , tExp
                  , tlbExit
                  }
             );

            tb.ButtonClick += new ToolBarButtonClickEventHandler(ToolBarButtonClick);


             tb.Dock = DockStyle.Top;
             Controls.Add(tb);
             Load += fLoad;
         }
          void fLoad(object sender, EventArgs a)
          {
             statStrip.Items[0].Text 
             = string.Format("{0} records has been red", dgv.Rows.Count);
          }

          void ToolBarButtonClick(object sender, ToolBarButtonClickEventArgs e)
          {
              string bNm = e.Button.Text;
             statStrip.Items[0].Text 
             = string.Format("You've pressed {0} button", bNm);

              if (bNm == "Exit")
                Close();
              else if (bNm == "Open")
                ;
              else {
                  MessageBox.Show("action not ready!","Warning");
              }

          }
    }
}

