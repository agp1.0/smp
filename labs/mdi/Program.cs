using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Drawing;
using dtGrd;

namespace mdi {
    class Program {
        static public Form  mWin;
        [STAThread]
        static void Main(string[] args) {
            mWin  =  new x();
            mWin.IsMdiContainer = true;    //<<<   
            Application.Run(mWin);
	    }
    }
    class x : Form  {
        StatusStrip statStrip;
        public x()        {
            Menu = new MainMenu();
            MenuItem FileIt = new MenuItem("File");
            MenuItem DataIt = new MenuItem("Data");
            MenuItem AboutIt = new MenuItem("About");
            MenuItem ExitIt = new MenuItem("Exit");

            MenuItem SupplierIt = new MenuItem("Supplier");
            MenuItem PartIt     = new MenuItem("Part");
            MenuItem ProjectIt  = new MenuItem("Project");
            MenuItem DeliveryIt = new MenuItem("Delivery");
            MenuItem WinIt      = new MenuItem("Windows");
            WinIt.MdiList =true;     //<<<

            FileIt.MenuItems.Add(ExitIt);

            AboutIt.Click += aboutF;
            ExitIt .Click += exitF;

            SupplierIt.Click += SupplierF;
            PartIt    .Click += PartF    ;
            ProjectIt .Click += ProjectF ;
            DeliveryIt.Click += DeliveryF;

            DataIt.MenuItems.Add(SupplierIt);
            DataIt.MenuItems.Add(PartIt);
            DataIt.MenuItems.Add(ProjectIt);
            DataIt.MenuItems.Add(DeliveryIt);

            Menu.MenuItems.Add(FileIt);
            Menu.MenuItems.Add(DataIt);
            Menu.MenuItems.Add(WinIt);
            Menu.MenuItems.Add(AboutIt);
             statStrip = new StatusStrip();
            ToolStripStatusLabel 
            statLabel = new ToolStripStatusLabel();
            statStrip.Items.Add(statLabel);           
          
            statLabel = new ToolStripStatusLabel();
            statStrip.Items.Add(statLabel);           ///
            Controls.Add(statStrip);
            statStrip.Items[0].Text = "���� ������";
            statStrip.Items[1].Text = "�������� ���� ���";
        }                                                        
                                                                 
        bool shouldIOpen (string text) {                    
            for (int i = 0; i < MdiChildren.Count(); i++){                                              
                if (this.MdiChildren[i].Name == text) {                                          
                    MdiChildren[i].Activate();             
                    return false;                          
                }                                          
            }                                              
            return true;                                   
        }                                                  
        void SupplierF(object sender, EventArgs a) {
            Console.WriteLine("����������");
            string id = "supplier";
            if (shouldIOpen(id)) {
                statStrip.Items[1].Text = "���� ����������� �� ������";
                Form z = new wnd2(
                 Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) 
                  +"/s.csv");
                z.MdiParent  = Program.mWin;        //<<<
                z.Text  = "����������";
                z.Name  = id;
                z.Show();
	    }
            statStrip.Items[1].Text = "���������� ���������";
        }
        void PartF(object sender, EventArgs a) {
            Console.WriteLine("������");
            string id = "part";
            if (shouldIOpen(id)) {
            statStrip.Items[1].Text = "���� ������� �� ������";
            Form z = new wnd2(
             Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) 
              +"/p.csv");
            z.MdiParent  = Program.mWin;
            z.Text  = "������";
            z.Name  = id;
            z.Show();
            }
            statStrip.Items[1].Text = "������ ���������";
        }
        void ProjectF(object sender, EventArgs a) {
            Console.WriteLine("�������");
            string id = "project";
            if (shouldIOpen(id)) {
            statStrip.Items[1].Text = "���� �������� �� ������";
            Form z = new wnd2(
             Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) 
              +"/j.csv");
            z.MdiParent  = Program.mWin;
            z.Text  = "�������";
            z.Name  = id;
            z.Show();
            }
            statStrip.Items[1].Text = "������� ���������";
        }

        void DeliveryF(object sender, EventArgs a) {
            Console.WriteLine("��������");
            statStrip.Items[1].Text = "���� �������� �� ������";
            string id = "delivery";
            if (shouldIOpen(id)) {
              Form z = new wnd2(
               Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) 
                +"/spj.csv");
              z.MdiParent  = Program.mWin;
              z.Text  = "��������";
              z.Name  = id;
              z.Show();
            }
            statStrip.Items[1].Text = "�������� ���������";
        }
        void aboutF(object sender, EventArgs a) {
            Console.WriteLine("about");
            statStrip.Items[1].Text = "������� ���� About";
            MessageBox.Show("This programm was made by ImiaRek, Kiev 201?.");
            statStrip.Items[1].Text = "�������� ���� ���";
        }
        void exitF(object sender, EventArgs x)  {
            Console.WriteLine("FormClosing");
            //ClickEvent FormClosing = FormClosedEventHandler;
            MessageBox.Show("exit");
            Close();
        }
    }
}
