using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace lab3 {
    class Program {
        [STAThread]
        static void Main(string[] args) {
            Console.WriteLine("hello");
            Form x = new x(1);
            Application.Run(x);
        }
        class x : Form {
            public x(int a) {
                Menu = new MainMenu();
                MenuItem FileIt = new MenuItem("File");
                MenuItem AboutIt = new MenuItem("About");
                MenuItem ExitIt = new MenuItem("Exit");

                Menu.MenuItems.Add(FileIt);
                Menu.MenuItems.Add(AboutIt);
                FileIt.MenuItems.Add(ExitIt);

                AboutIt.Click += aboutF;
                ExitIt .Click += exitF;
                FileIt.Click += FileF;

                Menu.MenuItems.Add(FileIt);
                Menu.MenuItems.Add(AboutIt);
                Menu.MenuItems.Add(ExitIt);

            }
            void aboutF(object sender, EventArgs a) {
                Console.WriteLine("about");
                MessageBox.Show("This programm was made by Bahrii Alyona PM-251, Kiev 2018.");
            }
            void exitF(object sender, EventArgs x) {
                Console.WriteLine("FormClosing");
                //ClickEvent FormClosing = FormClosedEventHandler;
                MessageBox.Show("exit");
            }
            void FileF(object sender, EventArgs a) {
                Console.WriteLine("file");
                MessageBox.Show("Save");
            }
        }
    }
}