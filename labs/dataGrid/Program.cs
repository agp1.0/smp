﻿//
#define WND
using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace dtGrd{
    class Program     {
        [STAThread]
        static void Main(string[] input)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
               // тут выясняется каталог, в котором выполняется приложение
            openFileDialog1.InitialDirectory 
              = Path.GetDirectoryName(
                  Assembly.GetExecutingAssembly().Location
                );
            if (openFileDialog1.ShowDialog() == DialogResult.OK)             {
                string fnm = openFileDialog1.FileName;
#if WND
                Form y = new wnd(fnm);
#else
                Form y = new wnd2(fnm);
#endif 
                Application.Run(y);
            }
        }                                                            
    }
}

