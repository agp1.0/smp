using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace dtGrd
{
    public class wnd : Form     {
         string fileName;
         protected
         DataGridView dgv = new DataGridView();

         public  wnd(string fnm)          {
           fileName = fnm;
           DataGridViewColumn clm = new DataGridViewColumn();
           clm.Name = "Col 1";

           dgv.ReadOnly = true;
           dgv.Dock = DockStyle.Fill;
           dgv.AllowUserToAddRows  = false;
           dgv.BackgroundColor     = Color.White;
           dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
           Load += fLoad;
           Controls.Add(dgv);
       }

       void fLoad(object sender, EventArgs a)       {
           Console.WriteLine("Load");
           ReadFile(fileName);
       }

       public void ReadFile(string path) {
           string str;
           string[] fields;
           bool firsttime = true;
           using (StreamReader sr = new StreamReader(path, Encoding.GetEncoding (1251)))        {
               while ((str = sr.ReadLine()) != null)                  {
                   //пропустить решетку
                   fields = str.Split(';');
                   if (fields.Length <= 0)
                       continue;
                   if (firsttime){
                       dgv.ColumnCount = fields.Length;
                       for (int i = 0; i < fields.Length; i++) {
                           dgv.Columns[i].Name = String.Format("Pole{0}", i+1);
                       }
                       firsttime = false;
                   }
                   dgv.Rows.Add(fields);
               }
           } 
       }
    }
}

/*

       private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs a)
       {
           Console.WriteLine("header click{0}, {1}", a.ColumnIndex, dgv.Columns[a.ColumnIndex].Name);
           Console.WriteLine("nomer: {0}", a.RowIndex);
       }
       void dgv_Click(object sender, DataGridViewCellMouseEventArgs a)
       {
           Console.WriteLine("header click{0}, {1}", a.ColumnIndex, dgv.Columns[a.ColumnIndex].Name);
           Console.WriteLine("nomer: {0}", a.RowIndex);
       }
*/