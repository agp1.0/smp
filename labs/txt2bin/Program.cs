using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            using (BinaryWriter bw = new BinaryWriter(File.Open("1.bin", FileMode.Create)))
            {
                Program x = new Program();
                x.str2bin ( Console.In, bw);

            }
        }
         void str2bin(TextReader tr, BinaryWriter bw)
        {
            int a;
            int b;
            string r;
            string[] sep = { " ", "\t" }; // separators 

           while ((r = tr.ReadLine())!=null){
            string[] flds = r.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            if (flds.Length >= 2)
            {
                a = int.Parse(flds[0]);
                b = int.Parse(flds[1]);
                bw.Write(a);
                bw.Write(b);
            }

          }
        }

    }
}
