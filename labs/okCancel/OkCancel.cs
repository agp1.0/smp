using System;
using System.Windows.Forms;
using System.Drawing;

namespace ConsoleApplication5{
       class OkCancel:Form       {
        public OkCancel(string qwert)          {
            Text = qwert;
            int p = 10;
            this.Padding = new Padding (p,p,p,p);

            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.ControlBox = false;
            this.AutoScroll = false;
            this.AutoSize = true;
            this.Size = new Size(10, 10);
            
           Panel p0 = new Panel ( ); // ��� ������ ������ ������ ������
            p0.Size = new Size (1, 32);
            p0.Dock = DockStyle.Top;
            p0.BorderStyle = BorderStyle.Fixed3D;
            p0.BackColor = Color.Green;
            Controls.Add (p0);

            Button butOk = new Button ();
            butOk.Size = new Size( 112,32);
            butOk.Dock = DockStyle.Right;
            butOk.Text = "Ok";
            Controls.Add(butOk);

            p0 = new Panel(); // ��� ������ ������ ���������� ����� ��������
            p0.Size = new Size( 112,32);
            p0.Dock = DockStyle.Right;
            p0.BorderStyle = BorderStyle.Fixed3D;
            p0.BackColor = Color.Red;
            Controls.Add(p0);
            
            Button butCancel = new Button ();
            butCancel.Size = new Size( 112,32);
            butCancel.Dock = DockStyle.Right;
            butCancel.Text = "Cancel";
            Controls.Add(butCancel);
           
            butOk.DialogResult = DialogResult.OK;
            butCancel.DialogResult = DialogResult.Cancel;
         

            this.DialogResult = DialogResult.Cancel;
            this.AcceptButton = butOk;
            this.CancelButton = butCancel;
            this.StartPosition = FormStartPosition.CenterScreen;
        }
    }
}
