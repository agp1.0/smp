using System;

class p{
    static void Main()    {
       byte b = 255;
       byte c = unchecked(b++); // � ��������� unchecked
       b=255;                   //  ����������� ������ �������� ����������
       unchecked {  //  ���� ����������
            b=255;   //  � ��������� unchecked  �����������
       	    b++;    //  ��� ���������
            b=255;   //
       	    b++;    //
       }
       b=255;   //
       b++;    //    exception is here
   }
}