// #define INTERLOCKED
// #define MONITOR
//#define LOCK
//
#define LOCKSTATIC
                                         
using System;
using System.IO;
using System.Threading;
namespace test {
    class m {
        static public int    max = 5;       // ���� ������ ��������
        static public int    sNumbers = 0;  ///< ���������� ����� �����, ����������� ������
        public int numbers = 0;
        [STAThread]                
        public static void Main() {
              m  someMemory      = new m(); // ������ ����� ��� ����� �������
              p b = new p("ThreadPriority.Lowest " , someMemory);
              p a = new p("ThreadPriority.Highest", someMemory,  ThreadPriority.Highest);

              b.t.Join();                   //  ������� ���������� ������ b
              a.t.Join();                   //  ������� ���������� ������ a
              string s;
#if  INTERLOCKED
              s = "interlocked";
#elif   MONITOR
              s = "monitor    ";
#elif   LOCK
              s = "lock       ";
#elif   LOCKSTATIC
              s = "lockStatic ";
#else

#error  You have to set at least one macro

#endif
              Console.WriteLine ("\n--{0} version, total(nonstatic/static): {1}/{2}\n" 
                 , s
                   ,  someMemory.numbers
                   ,  m.sNumbers
                   );
              Console.WriteLine ("thread '{0}' finished with {1} numbers"
                        , a.name, a.numbers);
              Console.WriteLine ("thread '{0}' finished with {1} numbers"
                          , b.name, b.numbers);
              Console.WriteLine ("\n--{0} version, difference(nonstatic/static): {1}/{2}\n" 
                 , s
                   ,  someMemory.numbers -  a.numbers - b.numbers
                   ,  m.sNumbers         -  a.numbers - b.numbers
                   );
        }
    }

    class p {
      public Thread t       = null; 
      public int    numbers = 0; 
      public string  name   = null;

      public p (string o
                  , m  sharedMemory  
                    ,   ThreadPriority p=ThreadPriority.Lowest
      ) {
         name       = o;
         t          =  new Thread(work); 
         t.Priority = p;
         t.Start(sharedMemory);     // �������� ����� ������ � �����       
      }

      public  void work(object o) {       
        m  mm = (m) o;              // �������� ����� ������ � ������
        DateTime st = DateTime.Now;
        for (DateTime cur = DateTime.Now
               ; (cur - st).TotalSeconds < m.max
                  ;  cur = DateTime.Now)  {
#if  INTERLOCKED
          Interlocked.Increment(ref mm.numbers);
#elif   MONITOR
          Monitor.Enter (mm);
          mm.numbers  ++;
          Monitor.Exit (mm);
#elif   LOCK
          lock(mm) {                //  ���������� ���� �����, � �� ���������
              mm.numbers++  ;
          }
#elif   LOCKSTATIC
          lock(typeof(m)) {
            m.sNumbers      ++;     // ��� ����� ����������
          }
#endif 
          numbers++;                // ��� ���������� �����, ����� ������
        }                           // ��� ������ �������
      }
    }
}
