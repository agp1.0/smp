using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace module2_p {
    class Program : Form     {
        [STAThread]
        static void Main(string[] args) {
            Application.Run(new Program());
        }

        List<Point> points = new List<Point>();

        Panel canvas;
        public Program() {
            canvas = new Panel();
            canvas.Dock = DockStyle.Fill;
            canvas.BackColor = Color.Gray;
            Controls.Add(canvas);
            canvas.MouseClick += Canvas_MouseClick;
            canvas.Paint      += _paint;
        }

        void _paint (object sender, PaintEventArgs e) {
             Graphics G = e.Graphics;
             if (points.Count >= 3) {
                G.FillPolygon(new SolidBrush(Color.Blue), points.ToArray());
                Console.WriteLine("count: {0}", points.Count);
             }
        }
        void Canvas_MouseClick(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                points.Add(new Point(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y));
                canvas.Invalidate();
            }
            else if (e.Button == MouseButtons.Right) {
                Point click = new Point(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y);
                if (AngleTest(click, points))
                    Console.WriteLine("in");
                else
                    Console.WriteLine("out");
            }
        }
        bool AngleTest(Point visitor, List<Point> figure) {
            double a = 0;
            for (int i = 0; i < figure.Count - 1; i++)
                a -= Angle(figure[i], visitor, figure[i + 1]);

            a -= Angle(figure[figure.Count - 1], visitor, figure[0]);
            Console.WriteLine("sum: {0}", a);

            if (Math.Abs((Math.Abs(a) - 360)) < 0.1) return true;
            else return false;
        }
        double Angle(Point A, Point B, Point C) {
            PointF vector1 = new PointF(A.X - B.X, A.Y - B.Y);
            PointF vector2 = new PointF(C.X - B.X, C.Y - B.Y);
            double sin = vector1.X * vector2.Y - vector2.X * vector1.Y;
            double cos = vector1.X * vector2.X + vector1.Y * vector2.Y;
            return Math.Atan2(sin, cos) * (180 / Math.PI);
        }
    }
}
