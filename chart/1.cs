using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.DataVisualization;
namespace MethodObch_L1 {
    static class Program {
        [STAThread]
        static void Main() {
            Application.Run(new Form1());
        }
    }
}



class Form1 : Form {
    public Form1() {
        //������� ������� Chart
        Chart myChart = new Chart();
        //������ ��� �� ����� � ����������� �� ��� ����.
        Controls.Add(myChart);     //  1 ������ �������� ���.������� �� �����

//        myChart.Parent = this;   /// 2 ������ �������� ���� �� �����
        myChart.Dock = DockStyle.Fill;
        //��������� � Chart ������� ��� ��������� ��������, �� ����� ����
        //�����, ������� ���� �� ���.
        myChart.ChartAreas.Add(new ChartArea("Math functions"));
        //������� � ����������� ����� ����� ��� ��������� �������, � ���
        //�� ����� ������� ��� �������, �� ������� ����� ���������� ����
        //����� �����.
        Series mySeriesOfPoint = new Series("Sinus");
        mySeriesOfPoint.ChartType = SeriesChartType.Line;
        mySeriesOfPoint.ChartArea = "Math functions";
  
        for (double x = -Math.PI; x <= Math.PI; x += Math.PI / 10.0)         {
            mySeriesOfPoint.Points.AddXY(x, Math.Sin(x));
        }

        Series mySeriesOfPoint1 = new Series("Cosinus");
        mySeriesOfPoint1.ChartType = SeriesChartType.Line;
        mySeriesOfPoint1.ChartArea = "Math functions";
        
        for (double x = -Math.PI; x <= Math.PI; x += Math.PI / 10.0)        {
            mySeriesOfPoint1.Points.AddXY(x, Math.Cos(x));
        }
        //��������� ��������� ����� ����� � Chart
        myChart.Series.Add(mySeriesOfPoint);
        myChart.Series.Add(mySeriesOfPoint1);
    }
}