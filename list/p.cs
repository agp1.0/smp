using System;
using System.IO;
using System.Collections.Generic;

class Program {
  static int Main() { 
      List <char> ls = new List<char> ();
      char [] x = {'z','w','o'};
      ls.AddRange (x);
      ls.AddRange (new char[] {'h','o','p','a'});
      ls.Add('r');

      Console.WriteLine ("count/index of w/4th elem: {0}/{1}/'{2}'"
         ,ls.Count, ls.IndexOf('w'), ls[4]);
      ls.Remove('o');
      ls.Sort();
      Console.WriteLine ("count/index of w/4th elem: {0}/{1}/'{2}'"
         ,ls.Count, ls.IndexOf('w'), ls[4]);
      ls.Reverse();
      foreach (char e in ls)
          Console.Write("'{0}' ", e);
// answer is
//count/index of w/4th elem: 8/1/'o'
//count/index of w/4th elem: 7/5/'r'
//'z' 'w' 'r' 'p' 'o' 'h' 'a'
      return 0;
}}

