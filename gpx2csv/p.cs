using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Diagnostics;
namespace test {
    class Program {
        static void Main(string[] args) {
           string flNm   = "small.gpx";
           int rc = 1;

           DateTime st = DateTime.Now;
           string s = "csv file is ready!";
           {
             if (File.Exists(flNm))  //если присуствует файл с траекторией отображаем ее на карте
             {
               string p = Path.GetDirectoryName(flNm);
               if (p.Length < 1) p = ".";
               string  outFile = p
                +"/"+Path.GetFileNameWithoutExtension(flNm)
                +".{0}.csv";

               StreamWriter sw = null;
               Console.Error.WriteLine(
                     "output file name if '{0}'!", outFile);
               	XmlDocument xml = new XmlDocument();
	        xml.Load(flNm);
                XmlNode gpx = xml.DocumentElement;
                XmlNodeList  lst  = gpx.ChildNodes; 
                XmlNodeList  trkLst = null; 
                Console.Error.WriteLine( 
                       "metadata '{0}'/{1}!", lst[0].Name, lst[0].InnerText);
                int i = 0;
                string tmp;
                for ( i = 1; i <lst.Count; i++) {
                   XmlNode trk = lst[i];
                   trkLst = trk.ChildNodes;
                   if (trkLst.Count > 0 ){
                     string ff;
                     if (trkLst[0].Name == "name"){                     // начался новый трек
                        Console.Error.WriteLine(
                            "next track/file: {0}/'{1}'", trkLst[0].InnerText
                             , string.Format(outFile,trkLst[0].InnerText.Replace(" ","_")));
                         if (sw !=null)
                            sw.Dispose();
                         sw = new StreamWriter(string.Format(outFile,trkLst[0].InnerText.Replace(" ","_")));
                         if(sw !=null) {
                            Console.Error.WriteLine( 
                                "stream writer is opened");
                            sw.WriteLine( 
                                "#stream writer is opened");
                         }
                     }
                   }
                   if (trkLst.Count > 1  && trkLst[1].Name == "trkseg"){  // новый сегмент трека
                     XmlNodeList  pnts  = trkLst[1].ChildNodes; 
                     if (pnts.Count > 0) {
                       XmlNode pnt1 = pnts[0];
                       if (pnt1.Name == "trkpt") {
                          if (sw!=null)
                             sw.Write("#    ");
                          else 
                             Console.Write("#    ");

                          foreach( XmlAttribute a in pnt1.Attributes){
                             if (sw!=null)
                                sw.Write("{0}    ", a.Name);
                             else 
                                Console.Write("{0}    ", a.Name);
                          }
                          foreach ( XmlNode pp in pnt1.ChildNodes) {
                             if (sw!=null)
                                sw.Write("{0}    ", pp.Name);
                             else 
                                Console.Write("{0}   ", pp.Name);
                          }
                       }
                       foreach( XmlNode pnt in pnts) {
                          if (pnt.Name == "trkpt") {
                             if (sw!=null)
                                sw.WriteLine("");
                             else 
                                 Console.WriteLine("");
                             foreach( XmlAttribute a in pnt.Attributes){
                                 if (sw!=null)
                                    sw.Write("{0}  ", a.InnerText);
                                 else 
                                    Console.Write("{0}  ", a.InnerText);
                             }
                             foreach ( XmlNode pp in pnt.ChildNodes) {
                                 if (pp.Name == "time")                            {
                                    tmp = pp.InnerText.Replace("T", " ").Replace("Z", " ");
                                    Console.Error.WriteLine(
                                          "time: {0}", tmp);
                                  }
                                  else 
                                    tmp = pp.InnerText;
                                 if (sw!=null)
                                    sw.Write("{0}  ", tmp);
                                 else 
                                    Console.Write("{0} ",  tmp);
                             }
                          }
                       }
                       Console.Write("\n");
                     }
                   }
                   if (sw !=null)
                      sw.Dispose();
                }
                if (trkLst == null || trkLst.Count < 1){
                  Console.Error.WriteLine(
                        "no any track");
                }
             }
             else {
               Console.Error.WriteLine( 
                     "no such file: '{0}'!", (string)flNm);
             }
             Console.WriteLine(s);
             DateTime fn = DateTime.Now; 
             Console.Error.WriteLine( "time of work is {0} secs;"
                  , (fn - st).TotalSeconds);
           }
       }                
}}          
