using System;
using System.Collections.Generic;
using System.IO;

namespace ToBin {
   class r2: X {
      double a;
      double b;
      public r2(): this(0.0,0.0) {}

      public r2(double x, double y) {
        a=x;
        b=y;
      }
      override
      public int CompareTo(X o) {
         return a.CompareTo(((r2)o).a);
      }
      override
      public X str2rec(string rec) {
          string [] flds =   rec.Split(sep, StringSplitOptions.RemoveEmptyEntries);
          if (flds.Length  >= 2) {
             a = double.Parse(flds[0]);
             b = double.Parse(flds[1]);
          }
          return new r2(a, b);
      }
      override
      public void rec2bin(BinaryWriter bw) {
         bw.Write(a);
         bw.Write(b);
      }
    }
}