using System;
//
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections;
using System.IO;

namespace ToBin {
   abstract
   class X:IComparable<X> {
      static        
      protected string[] sep = { " ", "\t" };     // separators
     abstract
     public X str2rec(string rec);

     abstract
     public void rec2bin(BinaryWriter bw);
    
      virtual
     public int CompareTo(X  o) {
        return 0;
     }
   }
   class Program {
       static void Main(string[] args) {
     //
         X rec = new i2();
     //    X rec = new r2();
         using (BinaryWriter bw = new BinaryWriter(File.Open("1.bin", FileMode.Create))){
            txt2bin ( Console.In, bw, true, rec);
         }
       }
       static
       void txt2bin (TextReader tr
          , BinaryWriter bw, bool sort, X rec ) {
          string r;
          List <X> ls = new List<X> ();
          while ((r = tr.ReadLine())!=null) {
            ls.Add(rec.str2rec(r));
          }
          if (sort)
             ls.Sort();
          for (int i = 0; i < ls.Count; i++)
             ls[i].rec2bin(bw);
       }
   }
}  
