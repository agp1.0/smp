using System;
using System.Collections.Generic;
using System.IO;

namespace ToBin {
   class i2: X {
      int a;
      int b;
      public i2(): this(0,0) {}
      public i2(int x, int y) {
        a=x;
        b=y;
      }
      override
      public int CompareTo(X o) {
         return a.CompareTo(((i2)o).a);
      }
      override
      public X str2rec(string rec) {
          string [] flds =   rec.Split(sep, StringSplitOptions.RemoveEmptyEntries);

          if (flds.Length  >= 2) {
             a = int.Parse(flds[0]);
             b = int.Parse(flds[1]);
          }
          return new i2(a, b);
      }
      override
      public void rec2bin(BinaryWriter bw){
         bw.Write(a);
         bw.Write(b);
      }
    }
}