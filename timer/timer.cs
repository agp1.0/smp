using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

class MMTimersTest {
    [DllImport("winmm.dll")]
    private static extern uint timeBeginPeriod(uint period);
    [DllImport("winmm.dll")]
    private static extern uint timeEndPeriod(uint period);
  
    static void Main(string []agrs) {
        if (agrs.Length < 1) { 
            Console.WriteLine(
             "usage:\n timer XXX\n where XXX msecs - period of sleeping (1000 times)"); 
        }
        else if (agrs[0] == "-?") { 
            Console.WriteLine(
              "usage:\n timer XXXX\n where XXXX msecs - period of sleeping"); 
        }
        else {
            int l = Int32.Parse(agrs[0]);
            uint period = 1;
            uint rc = timeBeginPeriod(period); //<<<<<<====
            Stopwatch stopWatch = new Stopwatch(); stopWatch.Start();
            for (int i = 1; i <= 1000; i++)  // ������ ����
                Thread.Sleep(l);
            stopWatch.Stop();
            rc = timeEndPeriod(period);        //<<<<<<====
            Console.WriteLine(" timeEndPeriod, rc = {0}", rc);

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime 
               = String.Format("{0:0.000} secs",ts.TotalMilliseconds/1000 );
            Console.WriteLine("RunTime with non standart tick: " + elapsedTime);

            Stopwatch stopWatch1 = new Stopwatch(); stopWatch1.Start();
            for (int i = 1; i <= 1000; i++)    
                Thread.Sleep(l);
            stopWatch1.Stop();
            ts = stopWatch1.Elapsed;
            elapsedTime 
              = String.Format("{0:0.000} secs" , ts.TotalMilliseconds/1000 );
            Console.WriteLine("RunTime with standart tick:" + elapsedTime);
        }
    }
}
